import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Task } from '../models/task.model';
import { List, ListModel } from '../models/list';
import {Observable, throwError} from 'rxjs';
import {catchError, retry} from 'rxjs/operators';
import {BaseService} from './base.service';

@Injectable({
  providedIn: 'root'
})

export class Service {

  constructor(private http: HttpClient, private baseService: BaseService) { }

  // Http Headers
  httpOptions = this.baseService.options;
  // base url
  baseUrl = this.baseService.baseUrl;
  // Error handler
  handleError = this.baseService.handleError;
  // GET ALL TASK LIST
  GetList(): Observable<List[]> {
    return this.http.get<List[]>(this.baseUrl + `/lists/`)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  // PUT List
  UpdateListPosition(data) {
      return this.http.put(this.baseUrl + `/lists/position`, JSON.stringify(data), this.httpOptions)
        .pipe(
          retry(1),
          catchError(this.handleError)
        );
  }
  //
  // PUT
  UpdateTask(id, data): Observable<Task> {
    return this.http.put<Task>(this.baseUrl + '/tasks/' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
}
