import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Task, TaskModelToEdit} from '../models/task.model';
import {catchError, retry, map} from 'rxjs/operators';
import { Employee } from '../models/employee.model';
import {Checklist} from '../models/Checklist.model';
import {TodoItem, TodoItemModel} from '../models/TodoItem.model';
import {BaseService} from './base.service';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient, private baseService: BaseService) { }

  // Http Headers
  httpOptions = this.baseService.options;
  // Error handle
  handleError = this.baseService.handleError;
  // Base URL
  baseUrl = this.baseService.baseUrl;
  //
  // GET EMPLOYEES
  GetEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(this.baseUrl + '/employees/', this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  // GET EMPLOYEE
  GetEmployee(id: number): Observable<Employee> {
    return this.http.get<Employee>(this.baseUrl + '/employees/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  //
  // POST
  CreateTask(data): Observable<Task> {
    return this.http.post<Task>(this.baseUrl + '/tasks', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // GET TASKS
  GetTask(id): Observable<Task> {
    return this.http.get<Task>(this.baseUrl + '/tasks/ ' + id, this.httpOptions)
      .pipe(map(data => data));
  }

  // DELETE
  DeleteTask(id) {
    return this.http.delete<Task>(this.baseUrl + '/tasks/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // PUT
  UpdateTaskName(data): Observable<TaskModelToEdit<string>> {
    return this.http.put<TaskModelToEdit<string>>(this.baseUrl + '/tasks/name', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  // PUT
  UpdateTaskStartDate(data): Observable<TaskModelToEdit<Date>> {
    return this.http.put<TaskModelToEdit<Date>>(this.baseUrl + '/tasks/startdate', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  // PUT
  UpdateTaskDuaDate(data): Observable<TaskModelToEdit<Date>> {
    return this.http.put<TaskModelToEdit<Date>>(this.baseUrl + '/tasks/duadate', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  // PUT
  UpdateTaskAssignEmployee(data): Observable<TaskModelToEdit<number>> {
    return this.http.put<TaskModelToEdit<number>>(this.baseUrl + '/tasks/assign', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  // PUT
  UpdateTaskOwner(data): Observable<TaskModelToEdit<number>> {
    return this.http.put<TaskModelToEdit<number>>(this.baseUrl + '/tasks/owner', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  CreateChecklist(data): Observable<Checklist> {
    return this.http.post<Checklist>(this.baseUrl + '/checklists' , JSON.stringify(data), this.httpOptions)
      .pipe(
          retry(1),
          catchError(this.handleError)
      );
  }

  // PUT
  UpdateChecklist(id, data): Observable<Checklist> {
    return this.http.put<Checklist>(this.baseUrl + '/checklists/' + id, JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // DELETE
  DeleteChecklist(id) {
    return this.http.delete<Checklist>(this.baseUrl + '/checklists/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
//
  //
  CreateTodoItem(data): Observable<TodoItem> {
    return this.http.post<TodoItem>(this.baseUrl + '/todoitems' , JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // PUT
  UpdateTodoItemName(data): Observable<TodoItemModel<string>> {
    return this.http.put<TodoItemModel<string>>(this.baseUrl + '/todoitems/name', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
  // PUT
  UpdateTodoItemStatus(data): Observable<TodoItemModel<boolean>> {
    return this.http.put<TodoItemModel<boolean>>(this.baseUrl + '/todoitems/status', JSON.stringify(data), this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }

  // DELETE
  DeleteTodoItem(id) {
    return this.http.delete<TodoItem>(this.baseUrl + '/todoitems/' + id, this.httpOptions)
      .pipe(
        retry(1),
        catchError(this.handleError)
      );
  }
}
