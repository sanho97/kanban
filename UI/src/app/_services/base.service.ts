import {Injectable} from '@angular/core';
import {JwtService} from './jwt.service';
import {HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {throwError} from 'rxjs';
import notify from 'devextreme/ui/notify';

@Injectable()
export class BaseService {
  AUTH_SCHEME = 'Bearer ';
  accessToken = this.jwtService.getToken();
  public appNotify: AppNotify;
  constructor(private jwtService: JwtService) {
    this.appNotify = new AppNotify();
  }

  // Base url
  baseUrl = 'https://localhost:44301/api';

  get headerAuthorizationKey(): string {
    return this.AUTH_SCHEME + this.accessToken;
  }

  get headers(): HttpHeaders {
    return new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'q=0.8;application/json;q=0.9',
      'Access-Control-Allow-Credentials': 'true',
      'Access-Control-Allow-Origin': location.origin,
    });
  }

  get options() {
    return {headers: this.headers};
  }

  // Error handling
  handleError(response: HttpErrorResponse) {
    //
    if (response.status === 403) {
      // this.appNotify.error('You don’t have permission to access [directory] on this server');
      notify('You don’t have permission to access [directory] on this server', 'error', 3000);
      return throwError(response);
    }
    //
    if (response.status === 500) {
      const error = response.error ? response.error.Message : response.statusText;
      if (!error) {
        // this.appNotify.error('Internal Server Error Message');
        notify('Internal Server Error Message', 'error', 3000);
      }
      // this.appNotify.error(error);
      notify(error, 'error', 3000);
      return throwError(response);
    }
    //
    let messageError = '';
    if (response.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      // console.error('An error occurred:', response.error.Message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${response.status}, ` +
        `body was: ${response.error}`);
    }

    if (!!response.error && !!response.error.Message) {
      messageError = response.error.Message;
    }

    // this.appNotify.error(messageError);
    notify(messageError, 'error', 3000);

    // return an observable with a user-facing error message
    return throwError(messageError);
  }
}

export class AppNotify {
  error(message: string) {
    notify(message, 'error', 3000);
  }
}
