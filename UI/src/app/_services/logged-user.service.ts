import {Injectable} from '@angular/core';
import {BehaviorSubject, Observable} from 'rxjs';
import {AuthenticationResult} from '../models/AuthenticationResult.model';

@Injectable()
export class LoggedUserService {
  // tslint:disable-next-line:variable-name
  private _currentUser: BehaviorSubject<AuthenticationResult> = new BehaviorSubject(null);

  constructor() {
  }

  get currentUser(): Observable<AuthenticationResult> {
    return this._currentUser.asObservable();
  }

  setLoggedUser(authenticationResult: AuthenticationResult) {
    this._currentUser.next(authenticationResult);
  }

  get loggedUserId(): number {
    const authenticationResult = this._currentUser.getValue();
    return authenticationResult ? authenticationResult.employee.id : null;
  }

  get loggedUser(): AuthenticationResult {
    return this._currentUser.getValue();
  }
}
