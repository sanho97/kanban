﻿import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {JwtService} from './jwt.service';
import {Employee} from '../models/employee.model';
import {BaseService} from './base.service';
import {AuthenticationResult} from '../models/AuthenticationResult.model';
import {LoggedUserService} from './logged-user.service';

@Injectable({providedIn: 'root'})
export class AuthenticationService {
  // Base url
  baseUrl = 'https://localhost:44301/api';
  // Error handle
  handleError1 = this.baseService.handleError;

  constructor(private http: HttpClient,
              private jwtService: JwtService,
              private baseService: BaseService,
              private loggedUserService: LoggedUserService) {
  }

  login(username, password): Observable<AuthenticationResult> {
    return this.http.post<AuthenticationResult>(this.baseUrl + `/auth/login`, {username, password});
  }

  register(email, password, name, address, phone) {
    const employee = new Employee();
    employee.name = name;
    employee.email = email;
    employee.password = password;
    employee.address = address;
    employee.phone = phone;

    return this.http.post(this.baseUrl + `/auth/register`, JSON.stringify(employee)).pipe(catchError(this.handleError1));
  }

  logout() {
    // remove user from local storage to log user out
    this.jwtService.destroyToken();
    this.removeCurrentUser();
  }

  setCurrentUser(authenticationResult: AuthenticationResult) {
    this.loggedUserService.setLoggedUser(authenticationResult);
  }

  removeCurrentUser() {
    this.loggedUserService.setLoggedUser(null);
  }

  getCurrentUser(): AuthenticationResult {
    return this.loggedUserService.loggedUser;
  }
}
