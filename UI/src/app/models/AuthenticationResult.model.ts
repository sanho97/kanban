import {Employee} from './employee.model';

export class AuthenticationResult {
  employee: Employee;
  accessToken: string;
}
