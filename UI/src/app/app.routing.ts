﻿import { Routes, RouterModule } from '@angular/router';
import {KanbanComponent} from './modules/kanban';
import { AuthGuard } from './_guards';
import {NgModule} from '@angular/core';
import {AuthComponent} from './modules/auth';

const appRoutes: Routes = [
    { path: '', component: KanbanComponent, canActivate: [AuthGuard] },
    { path: 'login', component: AuthComponent },

    // otherwise redirect to home
    { path: '**', redirectTo: '' }
];

// githup research: https://github.com/cornflourblue/angular-6-jwt-authentication-example.git
@NgModule({
  imports: [RouterModule.forRoot(appRoutes, {useHash: false})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
