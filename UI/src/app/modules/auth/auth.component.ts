import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthenticationService, JwtService, TaskService} from '../../_services';
import notify from 'devextreme/ui/notify';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})

export class AuthComponent implements OnInit {
  loginForm: FormGroup;
  loading = false;
  submitted = false;
  returnUrl: string;
  error = '';

  registerForm: FormGroup;
  submittedRegister = false;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService,
    private authenticationService: AuthenticationService,
    private jwtService: JwtService) {}

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    });

    this.registerForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
      address: ['', Validators.required],
      phone: ['', Validators.required],
      name: ['', Validators.required],
      rePassword: ['', Validators.required]
    });

    // reset login status
    this.authenticationService.logout();

    // get return url from route parameters or default to '/'
    this.returnUrl = this.route.snapshot.queryParams.returnUrl || '/';
  }

  // convenience getter for easy access to form fields
  get formLogin() { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.loading = true;
    this.authenticationService.login(this.formLogin.username.value, this.formLogin.password.value)
      .subscribe(
        (data) => {
          this.jwtService.saveToken(data.accessToken);
          this.router.navigate([this.returnUrl]);
          this.authenticationService.setCurrentUser(data);
          this.loading = false;
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }

  get formRegister() { return this.registerForm.controls; }

  onSubmitRegister() {
    this.submittedRegister = true;

    if (this.registerForm.invalid) {
      return;
    }

    this.loading = true;
    // register
    this.authenticationService.register(
      this.formRegister.email.value,
      this.formRegister.password.value,
      this.formRegister.name.value,
      this.formRegister.address.value,
      this.formRegister.phone.value)
      .subscribe(
        (data) => {
          notify('Sign Up Success!', 'success', 1000);
          this.loading = false;
        },
        error => {
          this.error = error;
          this.loading = false;
        });
  }
}
