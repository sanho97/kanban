import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {
    DxPopupModule,
    DxButtonModule,
    DxTemplateModule,
    DxValidationGroupModule,
    DxTextBoxModule,
    DxDateBoxModule, DxPopoverModule, DxTextAreaModule
} from 'devextreme-angular';
import { DxBoxModule } from 'devextreme-angular';
import { DxCheckBoxModule,
  DxSelectBoxModule,
  DxNumberBoxModule,
  DxFormModule } from 'devextreme-angular';

import { AppComponent } from './app.component';
import {DxScrollViewModule, DxSortableModule} from 'devextreme-angular';
import {Service, TaskService, AuthenticationService, JwtService, BaseService} from './_services';
import { TaskComponent } from './modules/kanban/task/task.component';
import { KanbanComponent } from './modules/kanban';
import {RouterModule} from '@angular/router';
import {ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from './app.routing';
import {AuthComponent} from './modules/auth';
import {HttpTokenInterceptor} from './_interceptors/http.token.interceptor';
import {LoggedUserService} from "./_services/logged-user.service";

@NgModule({
  declarations: [
    AppComponent,
    TaskComponent,
    KanbanComponent,
    AuthComponent
  ],
  imports: [
    BrowserModule,
    DxScrollViewModule,
    DxSortableModule,
    DxPopupModule,
    DxButtonModule,
    DxTemplateModule,
    DxBoxModule,
    DxCheckBoxModule,
    DxSelectBoxModule,
    DxNumberBoxModule,
    DxFormModule,
    HttpClientModule,
    DxValidationGroupModule,
    DxTextBoxModule,
    DxDateBoxModule,
    DxPopoverModule,
    DxTextAreaModule,
    RouterModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  // tslint:disable-next-line:max-line-length
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: HttpTokenInterceptor, multi: true },
    Service,
    TaskService,
    AuthenticationService,
    JwtService,
    HttpTokenInterceptor,
    BaseService,
    LoggedUserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
