﻿using Kanban.Model.Models.Auth;
using Kanban.Model.Shared.Service;
using Microsoft.Extensions.Options;
using System.Security.Claims;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System;
using System.Threading.Tasks;
using AutoMapper;

namespace Kanban.Service.Implements
{
    public class TokenAuthenticationService : IAuthenticateService
    {
        private readonly IEmployeeService _employeeService;
        private readonly TokenManagement _tokenManagement;
        private readonly IMapper _mapper;

        public TokenAuthenticationService(IEmployeeService employeeService, IOptions<TokenManagement> tokenManagement, IMapper mapper)
        {
            _employeeService = employeeService;
            _tokenManagement = tokenManagement.Value;
            _mapper = mapper;
        }

        public async Task<AuthenticationResult> Authenticated(TokenRequest request)
        {
            if (!await _employeeService.IsValidUser(request.Username, request.Password))
               throw new Exception("Invalid username or password");

            var claim = new[]
            {
                new Claim(ClaimTypes.Name, request.Username)
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_tokenManagement.Secret));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var jwtToken = new JwtSecurityToken(
                _tokenManagement.Issuer,
                _tokenManagement.Audience,
                claim,
                expires: DateTime.Now.AddMinutes(_tokenManagement.AccessExpiration),
                signingCredentials: credentials
            );

            var employee =  await _employeeService.GetEmployeeByEmail(request.Username);
            var employeeModel = _mapper.Map<EmployeeResutlModel>(employee);

            var authenticationResult = new AuthenticationResult(employeeModel, new JwtSecurityTokenHandler().WriteToken(jwtToken));

            return authenticationResult;
        }
    }
}
