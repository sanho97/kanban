﻿using AutoMapper;
using Kanban.Encryption;
using Kanban.Model.Entities;
using Kanban.Model.Models;
using Kanban.Model.Shared;
using Kanban.Model.Shared.Service;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kanban.Service.Implements
{
    public class EmployeeService : BaseService, IEmployeeService
    {
        private readonly IPasswordHasher _passwordHasher;
        private readonly IMapper _mapper;

        public EmployeeService(IUnitOfWork unitOfWork, IMapper mapper, IPasswordHasher passwordHasher) : base(unitOfWork)
        {
            _mapper = mapper;
            _passwordHasher = passwordHasher;
        }

        public async Task<Employee> GetEmployeeByEmail(string email) => await _unitOfWork.EmployeeRepository.GetEmployeeByEmail(email);

        public async Task<EmployeeModel> Add(EmployeeModel model)
        {
            var employeecheck = await GetEmployeeByEmail(model.Email);

            if (employeecheck != null)
                throw new Exception("This user already exists!");

            string saft = null;

            var hashCode = _passwordHasher.Hash(model.Password, saft);

            model.Password = hashCode.Value;
            model.Saft = hashCode.Saft;

            var employee = _mapper.Map<Employee>(model);

            return _mapper.Map<EmployeeModel>(await _unitOfWork.EmployeeRepository.AddAsyn(employee));     
        }

        public async Task<EmployeeModel> Get(int id) => _mapper.Map<EmployeeModel>(await _unitOfWork.EmployeeRepository.GetAsyn(id));

        public async Task<IEnumerable<Employee>> GetAll() => await _unitOfWork.EmployeeRepository.GetAllAsyn();

        public async Task<bool> IsValidUser(string email, string password)
        {
            var employee = await _unitOfWork.EmployeeRepository.GetEmployeeByEmail(email);

            if (employee == null)
            {
                return false;
            }

            var hashcode = _passwordHasher.Hash(password, employee.Saft);

            return await _unitOfWork.EmployeeRepository.IsValidUser(email, hashcode.Value);
        }
    }
}