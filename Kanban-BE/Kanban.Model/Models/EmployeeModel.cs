﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kanban.Model.ViewModels
{
    public class EmployeeViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
        public string Saft { get; set; }
    }
}
