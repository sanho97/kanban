﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kanban.Model.Models.Auth
{
    public class HashCode
    {
        public HashCode(string value, string saft)
        {
            Value = value;
            Saft = saft;
        }

        public string Value { get; set; }

        public string Saft { get; set; }
    }
}
