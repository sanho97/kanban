﻿using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations;

namespace Kanban.Model.Models.Auth
{
    public class TokenRequest
    {
        public TokenRequest(){}

        public TokenRequest(string username, string password)
        {
            Username = username;
            Password = password;
        }

        [Required]
        [JsonProperty("username")]
        public string Username { get; set; }


        [Required]
        [JsonProperty("password")]
        public string Password { get; set; }
    }
}
