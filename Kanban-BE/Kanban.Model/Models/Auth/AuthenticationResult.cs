﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kanban.Model.Models.Auth
{
    public class AuthenticationResult
    {
        public AuthenticationResult(EmployeeResutlModel employee, string accessToken)
        {
            Employee = employee;
            AccessToken = accessToken;
        }

        public EmployeeResutlModel Employee { get; set; }
        public string AccessToken { get; set; }
    }

    public class EmployeeResutlModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Phone { get; set; }
    }
}
