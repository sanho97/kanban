﻿using Kanban.Model.Models.Auth;
using System.Threading.Tasks;

namespace Kanban.Model.Shared.Service
{
    public interface IAuthenticateService
    {
        Task<AuthenticationResult> Authenticated(TokenRequest request);
    }
}
