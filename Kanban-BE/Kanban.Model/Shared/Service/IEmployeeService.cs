﻿using Kanban.Model.Entities;
using Kanban.Model.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kanban.Model.Shared.Service
{
    public interface IEmployeeService
    {
        Task<EmployeeModel> Get(int id);
        Task<IEnumerable<Employee>> GetAll();
        Task<bool> IsValidUser(string username, string password);
        Task<EmployeeModel> Add(EmployeeModel model);
        Task<Employee> GetEmployeeByEmail(string email);
    }
}
