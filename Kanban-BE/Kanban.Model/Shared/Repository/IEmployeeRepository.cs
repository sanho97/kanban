﻿using Kanban.Model.Entities;
using System.Threading.Tasks;

namespace Kanban.Model.Shared.Repository
{
    public interface IEmployeeRepository : IGenericRepository<Employee>
    {
        Task<bool> IsValidUser(string email, string password);
        Task<Employee> GetEmployeeByEmail(string email);
    }
}
