﻿using Kanban.Model.Entities;
using Kanban.Model.Shared.Repository;
using Kanban.Repository.Collections;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Kanban.Repository.Repositories
{
    public class EmployeeRepository : GenericRepository<Employee>, IEmployeeRepository
    {
        public EmployeeRepository(KanbanDBContext context) : base(context)
        {
        }

        public async Task<Employee> GetEmployeeByEmail(string email) => await _context.Employee.FirstOrDefaultAsync(_ => _.Email == email);

        public async Task<bool> IsValidUser(string email, string password)
        {
            var employee = await _context.Employee.FirstOrDefaultAsync(_ => _.Email == email && _.Password == password);

            if (employee == null) return false;

            return true;
        }
    }
}
