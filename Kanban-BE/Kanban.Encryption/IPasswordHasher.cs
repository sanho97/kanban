﻿namespace Kanban.Encryption
{
    public interface IPasswordHasher
    {
        Model.Models.Auth.HashCode Hash(string password, string _saft);
    }
}
