﻿using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using System;
using System.Security.Cryptography;

namespace Kanban.Encryption
{
    public class PasswordHasher : IPasswordHasher
    {
        public Model.Models.Auth.HashCode Hash(string password, string _salt)
        {
            byte[] salt;

            if (String.IsNullOrEmpty(_salt))
            {
                salt = new byte[128 / 8];

                using (var rng = RandomNumberGenerator.Create())
                {
                    rng.GetBytes(salt);
                }
                Console.WriteLine($"Salt: {Convert.ToBase64String(salt)}");

            }
            else
            {
                salt = Convert.FromBase64String(_salt);
            }

            // derive a 256-bit subkey (use HMACSHA1 with 10,000 iterations)
            string hashed = Convert.ToBase64String(KeyDerivation.Pbkdf2(
                password: password,
                salt: salt,
                prf: KeyDerivationPrf.HMACSHA1,
                iterationCount: 10000,
                numBytesRequested: 256 / 8));

            return new Model.Models.Auth.HashCode(hashed, Convert.ToBase64String(salt));
        }
    }
}
