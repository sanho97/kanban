﻿using Kanban.Model.Models;
using Kanban.Model.Models.Auth;
using Kanban.Model.Shared.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Kanban.Api.Controllers
{
    [Route("api/auth")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly IAuthenticateService _authService;
        private readonly IEmployeeService _employeeService;

        public AuthenticationController(IAuthenticateService authService, IEmployeeService employeeService)
        {
            _authService = authService;
            _employeeService = employeeService;
        }

        [AllowAnonymous]
        [HttpPost, Route("login")]
        public async Task<AuthenticationResult> Login([FromBody] TokenRequest request) =>  await _authService.Authenticated(request);
                
        [AllowAnonymous]
        [HttpPost, Route("register")]
        public async Task Register([FromBody] EmployeeModel model) => await _employeeService.Add(model);
    }
}