﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Kanban.Model.Entities;
using Kanban.Model;
using Kanban.Service.Implements;
using Kanban.Model.Shared;
using Kanban.Model.Shared.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Kanban.Model.Models.Auth;
using System.Text;
using Kanban.Encryption;
using Kanban.Model.Shared.Extensions;
using Microsoft.Extensions.Logging;
using NLog;
using System;
using System.IO;

namespace Kanban.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            LogManager.LoadConfiguration(String.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var ConnectionString = Configuration.GetConnectionString("KanbanDb");

            services.AddDbContext<KanbanDBContext>(options =>
                {
                    options.UseSqlServer(ConnectionString);
                    options.UseLazyLoadingProxies(false);
                });

            var config = new AutoMapper.MapperConfiguration(c => { c.AddProfile(new MapProfile()); });

            var mapper = config.CreateMapper();

            services.AddSingleton(mapper);

            services.AddScoped<IUnitOfWork, UnitOfWork.Repositories.UnitOfWork>();

            services.AddScoped<ITaskService, TaskService>();
            services.AddScoped<IListService, ListService>();
            services.AddScoped<IEmployeeService, EmployeeService>();
            services.AddScoped<IChecklistService, ChecklistService>();
            services.AddScoped<ITodoItemService, TodoItemService>();

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    builder =>
                    {
                        builder.WithOrigins("http://localhost:4200")
                            .AllowAnyMethod()
                            .AllowAnyHeader()
                            .AllowCredentials();
                    });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.Configure<TokenManagement>(Configuration.GetSection("tokenManagement"));
            var token = Configuration.GetSection("tokenManagement").Get<TokenManagement>();
            var secret = Encoding.ASCII.GetBytes(token.Secret);

            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(secret),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });

            services.AddScoped<IAuthenticateService, TokenAuthenticationService>();
            services.AddScoped<IPasswordHasher, PasswordHasher>();
            services.AddSingleton<ILoggerManager, LoggerManager>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerManager logger)
        {
            if (env.IsDevelopment())
            {
                //app.UseDeveloperExceptionPage();
                //app.ConfigureExceptionHandler(logger);
            }
            else
            {   
                app.UseHsts();
            }

            app.ConfigureCustomExceptionMiddleware();
            //
            // Allow http://localhost:4200  
            app.UseCors("CorsPolicy");
            app.UseAuthentication();
            app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
